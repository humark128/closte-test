<?php
$ai_header_name      = 'x-closte-ai';
$scan_id_header_name = 'x-closte-ai-scan-id';
$client_api_key      = 'C75Daw5,@xT@UDa7,JpH62fcbea03dd6c77b09262399';

if (function_exists('getallheaders')) {
    $headers = getallheaders();
    
    if (array_key_exists($ai_header_name, $headers) && array_key_exists($scan_id_header_name, $headers)) {
        
        if ($client_api_key == $headers[$ai_header_name]) {
            define('CLOSTE_AI_ENABLED', true);
            define('CLOSTE_AI_SCAN_ID', $headers[$scan_id_header_name]);
            ini_set('opcache.enable', '0');
        }
    }
}